const { assert } = require("chai");
const { checkAge, checkFullName } = require("../src/util");

describe("test_checkAge", () => {

    it("age_is_an_integer", () => {
        const age = 420.69;
        assert(checkAge(age) % 1 === 0, "age is not an integer");
    })
    
    it("age_is_a_number", () => {
        const age = "sixty-nine";
        assert.isNumber(checkAge(age), "age is not a number");
    })
    
    it("age_is_not_empty", () => {
        const age = "";
        assert.isNotEmpty(checkAge(age), "age is empty");
    })

    it("age_is_not_undefined", () => {
        const age = undefined;
        assert.isDefined(checkAge(age), "age is undefined");
    })

    it("age_is_not_null", () => {
        const age = null;
        assert.isNotNull(checkAge(age), "age is null");
    })

    it("age_is_not_equal_to_0", () => {
        const age = 0;
        assert.notStrictEqual(checkAge(age), 0, "age is equal to 0");
    })

})

describe("test_checkFullName", () => {

    it("fullName_is_not_empty", () => {
        const fullName = "";
        assert.isNotEmpty(checkFullName(fullName), "fullName is empty");
    })

    it("fullName_is_not_undefined", () => {
        const fullName = undefined;
        assert.isDefined(checkFullName(fullName), "fullName is undefined");
    })

    it("fullName_is_not_null", () => {
        const fullName = null;
        assert.isNotNull(checkFullName(fullName), "fullName is null");
    })

    it("fullName_is_a_string", () => {
        const fullName = 69;
        assert.isString(checkFullName(fullName), "fullName is a not string");
    })

    it("fullName_character_length_is_not_0", () => {
        const fullName = "";
        assert.notStrictEqual(checkFullName(fullName).length, 0, "fullName has a character length of 0");
    })

})