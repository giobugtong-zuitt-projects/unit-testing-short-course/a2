const checkAge = age => {
    if (age % 1 !== 0) return parseInt(age);
    if (isNaN(age)) return "Error: age is not a number";
    if (age === "") return "Error: age is empty";
    if (age === undefined) return "Error: age is undefined";
    if (age === null) return "Error: age is null";
    if (age === 0) return "Error: age is 0";
    return age;
}

const checkFullName = fullName => {
    if (fullName === "") return "Error: fullName is empty";
    if (fullName === undefined) return "Error: fullName is undefined";
    if (fullName === null) return "Error: fullName is null";
    if (typeof(fullName) !== "string") return "Error: fullName is not a string";
    if (fullName.length === 0) return "Error: fullName has a character length of 0";
    return fullName;
}

module.exports = {
    checkAge: checkAge,
    checkFullName: checkFullName
}